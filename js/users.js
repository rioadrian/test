const getUsers = () => {
    let str_ = "";
    var jqxhr = $.get("https://reqres.in/api/users?page=2", function() {
      })
        .done(function(response) {
            // console.log('response: ', response);
          if(response && response.data && response.data.length > 0) {
            str_ = "";
            let resp_string = response.data.map((dt) => {
                str_ += `<div class="row shadow-sm p-2 mb-1 bg-body" id="${dt.id}">
                            <div class="col-4 p-0">
                                <img src="${dt.avatar}" class="rounded w-75">
                            </div>
                            <div class="col-8 p-0">
                                <span class="nama-user">${dt.first_name} ${dt.last_name}</span>
                                <span class="email-user"><i
                                        class="fa fa-envelope"></i>${dt.email}</span>
                            </div>
                        </div>`;
            });
          } else {
            str_ = "";
            str_ += `<div class="alert alert-danger" role="alert">
              Record Not Found!
            </div>`;
          };

          str_ = `<div class="user-list-item">${str_}</div>`;
            // console.log('userlist: ', str_);
            $(".user-list-item").replaceWith(str_);
            return;
        })
        .fail(function(error) {
          alert( "error found when try to connect server!" );
        //   console.log('error: ', error);
        })
        .always(function() {
          
        });
       
      // Perform other work here ...
       
      // Set another completion function for the request above
      // jqxhr.always(function(response) {
      //   alert( "second finished" );
      //   console.log('response2: ', response);
      // });
};

$( document ).ready(function() {
    // clear list
    $(".user-list-item").replaceWith(`<div class="user-list-item"></div>`);
    // get list by calling API server
    getUsers();
});